# Node_API Challenge

Node API with Express and Postgres. https://github.com/ishan-me/Node_API. 

For building and running the application on Kubernetes, my commits contains Dockerfile, .gitlab_ci.yml, helm directory and notify.sh.

</br>

## Navigation

---
- [Node_API Challenge](#node_api_challenge)
  - [TL&DR](#tl&dr)
  - [Navigation](#navigation)
  - [Kubernetes](#kubernetes)
  - [Build](#build)
  - [Deployment](#deployment)
  - [Notifier](#notifier)
  - [Overview](#overview)
</br>

## TL&DR

To deploy the application you need to define some parameters for database connection. **Settings --> CI/CD --> Variables**. The variables should be seems like the image below. Also you need to define container registry credentials like dockerhub. You can also use your private container registry. Then go to **CI/CD --> Run Pipeline** to run pipeline.

<br>
</br>
<br>
</br>

<p align="center">
    <img height="450" src="docs/screenshot.jpg">
</p>


## Kubernetes

To create kubernetes clusters I used my own repository which is based on Ansible playbooks. **https://github.com/alperencelik/noobspray** the repository tested on Centos 7 VM's and for now the project doesn't support more than one master node since it requires lb/proxy solution. There is no limit for worker nodes and also you can add more nodes to scale up your cluster. For this project I created 3 different clusters with "**noobspray**" two of them for test and production environment and the third one for Gitlab runner. You should register your clusters for production and test environments. To register you can follow this link https://docs.gitlab.com/ee/user/project/clusters/add_existing_cluster.html. After adding clusters you can list them on **Admin --> Kubernetes**

<br>
</br>
<br>
</br>

<p align="center">
    <img height="200" src="docs/screenshot2.jpg">
</p>

<br>
</br>


## Build 

For building the source code I used a tool called Kaniko which builds container images from Dockerfile on containers or Kubernetes cluster. For more information about Kaniko you can check it out **https://github.com/GoogleContainerTools/kaniko**. At the end of build stage the container image will be pushed the container registry which is defined on build stage. For this project I pushed different branches with different names on container registry(for this case it's dockerhub). Since the container images doesn't include any confidential information I stored them as Public repository so we don't need to use any imagePullSecret to pull images from registry. 

<br>
</br>



## Deployment


Deployment of the application to Kubernetes is provided by helm. Templates, charts and values files can be found in **helm/** directory. It is stated that we should be able to change the database endpoints without building the container image so I create **helm/secret.yaml**. To skip the build stage of this project you should pass an environment variable before running the pipeline. If you define **skipbuild=true** the build and deploy stages will be skipped and only **secretfile** will be changed.

<p align="center">
    <img height="400" src="docs/screenshot3.jpg">
</p>

For sake of simplicity I created branches called "dev" and "master" which is stated on requirements on this study. On dev branch to build an application you should run pipeline manually, but on master branch build and deploy stages will move automatically. That means when we merge dev branch to master it will automatically build and deploy project. So that the two branches do not affect each other I designed two different Kubernetes cluster. With this solution if the test environment goes down there shouldn't be any impact on production environment. So building for dev branch goes manually but deploying for both branches goes automatically.


## Notifier

To run some checks I used kubectl commands in a bash script. Notifier works at the end of pipeline. It can be run in another time with the help of scheduled pipeline but for this project it only works as the end stage of pipeline. Notification endpoints can go up since there is too much things to check but I don't want to make it more complicated. The notifier stage runs the notify.sh on prebuilt kubectl image. For the output of commands the notifier sends a Slack message. For the Slack part you can follow this link https://api.slack.com/tutorials/tracks/posting-messages-with-curl

PS: You should add your Slack webhook on notify.sh

## Overview

All the CI/CD process taken care by Gitlab and runner kubernetes. Gitlab is like control center of this study. In addition to that for handling build and deploy workloads I used Kubernetes cluster as a runner. I was planning to use my own container registry but since I got problems about my Minio server I gave up and then used dockerhub. The simple schematic for the study can be shown below.

<br>
</br>




<p align="center">
    <img height="800" src="docs/screenshot4.jpg">
</p>



---


